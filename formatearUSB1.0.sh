#! /bin/bash
#
#################################################################################
#  ____________________________________________________
# |                                                    |
# | Format USB Script v1.0.2                           |
# |                                                    |
# | Formatea memorias USB como FAT32 o NTFS fácilmente |
# |____________________________________________________|
#
#--------------------------------------------------------------------------------
# Descripción
# ===========
# 
# Script para formatear memorias USB como FAT32 o NTFS desde terminal Linux
#
#
# Requisitos
# ==========
#
# Requiere tener instalados los paquetes sudo (sustituible por "su" modificando
# el script), dosfstools y ntfsprogs
#
#--------------------------------------------------------------------------------
# Copyright (C) 2013  Manuel de la Fuente
# GNU GPL v3.0
# http://manuelfte.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#--------------------------------------------------------------------------------
# 
# Este programa es libre de copiarse, distribuirse o modificarse respetando los
# términos de la licencia anterior. Favor de mantener autores, coautores y demás
# información intacta, añadiendo la suya si lo desea. En caso de realizar alguna
# mejora, estaría agradecido si me la hiciese saber.
#
#--------------------------------------------------------------------------------
#
##################################################################################

#Colores
BLUE="\033[1;34m"
GRAY="\033[1;30m"
GREEN="\033[1;32m"
PINK="\033[1;35m"
RED="\033[1;31m"
WHITE="\033[1;37m"
YELLOW="\033[1;33m"
ENDCOLOR="\033[0m"

echo -e $YELLOW"Format USB Script v1.0.2"$ENDCOLOR

sudo fdisk -l
echo -e $BLUE"Mostrando tabla de particiones y dispositivos de almacenamiento conectados."$ENDCOLOR

DEVICE=0
while [ ! -e /dev/$DEVICE ];do
    echo -e $WHITE'Elija la memoria a formatear (solo escriba lo que está después de "/dev/"):'$ENDCOLOR
    read DEVICE

    if [ -e /dev/$DEVICE ];then
        sudo umount /dev/$DEVICE

    else
        echo -e $RED"No existe el dispositivo indicado, inténtelo nuevamente."$ENDCOLOR

    fi
done

FORMAT=0
while [ $FORMAT != 1 -a $FORMAT != 2 ];do
    echo -e $WHITE"Elija el formato:"$ENDCOLOR
    echo -e $PINK"1) FAT32"$ENDCOLOR
    echo -e $PINK"2) NTFS"$ENDCOLOR
    read FORMAT

    if [ $FORMAT = 1 ];then
        PARAMS='mkfs.vfat -F 32 -n'
        FRMTNAME='FAT32'

    elif [ $FORMAT = 2 ];then
        PARAMS='mkntfs -f -L'
        FRMTNAME='NTFS'

    else
        echo -e $RED"Opción inválida, escriba 1 o 2."$ENDCOLOR

    fi
done

echo -e $WHITE"Escriba el nombre que desea para la memoria:"$ENDCOLOR
read NAME

echo -e $BLUE'Formateando /dev/'$DEVICE 'como "'$NAME'" en '$FRMTNAME', por favor, espere...'$ENDCOLOR
sudo $PARAMS "$NAME" /dev/$DEVICE
echo -e $GREEN"Formato terminado."$ENDCOLOR

# END
#################################################################################