#!/bin/bash
 
app_name="WebSite Downloader"
app_version="1.0.0"
app_author="Security Null"
app_last_update="2013-06-16"
app_usage="Usage: ./website_downloader.sh <website url> [compress 0|1] [verbose 0|1]"
app_example="Example: ./website_downloader.sh http://securitynull.net/login.php?user=username&pwd=password 1 1"
 
echo "$app_name $app_version ($app_author)"
echo
 
function download_website() {
    url=$1
    compress=$2
    if [ $3 -eq 1 ]; then
        verbose=""
    else
        verbose="--quiet"
    fi
    domain=$(echo "$url" | sed 's/https\?:\/\///')
    #domain=$(echo "$domain" | sed 's/www\.//g')
    uri="${domain#*\/|*}"
    domain="${domain%%\/*}"
    params="${uri#*\?}"
    uri=$(echo "$uri" | sed "s/\?$params//g")
    uri=$(echo "$uri" | sed "s/$domain//g")
    params=$(echo "$params" | sed "s!$domain/!!g")
    params=$(echo "$params" | sed "s!$domain!!g")
    website=$(echo "$url" | sed "s!$uri?$params!!g")
         
    echo " -WebSite URL:" $url
    echo " -Site:" $website
    echo " -Domain:" $domain
    echo " -URI:" $uri
    echo " -Params:" $params
    echo
 
    # Login
    if [ "$params" != "" ]; then
        echo " -Login WebSite..."
        wget ${website}${uri} $verbose --post-data "$params" --save-cookies $domain.cookie --no-check-certificate -O - > /dev/null
        if [ ${PIPESTATUS[0]} -eq 0 ]; then
            echo " -Cookie saved ($domain.cookie)"
        else
            echo " -Login ERROR"
            if [ -f $domain.cookie ]; then
                rm $domain.cookie
            fi
            exit 1
        fi
    fi
     
    # Download WebSite
    echo " -Downloading WebSite..."
    if [ "$params" != "" ]; then
        wget -rkcp -e robots=off -U Mozilla --limit-rate=80K --random-wait --domains $domain --html-extension $domain $verbose
    else
        wget -rkcp -e robots=off -U Mozilla --limit-rate=80K --random-wait --load-cookies $domain.cookie --domains $domain --html-extension $domain $verbose
    fi
     
    if [ ${PIPESTATUS[0]} -eq 0 ]; then
        echo " -Download Complete"
    else
        echo " -WebSite Download ERROR"
        exit 2
    fi
     
    # Compress
    if [ $compress -eq 1 ]; then
        echo " -Compressing WebSite..."
        tar -czvf $domain.tgz $domain/*
        if [ ${PIPESTATUS[0]} -eq 0 ]; then
            echo " -WebSite Compress ($domain.tgz)"
            if [ -f $domain.cookie ]; then
                rm $domain.cookie
            fi
            rm -r $domain
        else
            echo " -WebSite Compress ERROR"
            exit 3
        fi
    fi
     
    echo " -WebSite downloaded - FINISHED!!"
}
 
# Check params
if [ $# -eq 1 ]; then
    download_website $1 0 0
elif [ $# -eq 2 ]; then
    download_website $1 $2 0
elif [ $# -eq 3 ]; then
    download_website $1 $2 $3
else
    echo $app_usage
    echo
    echo $app_example
    exit 1
fi