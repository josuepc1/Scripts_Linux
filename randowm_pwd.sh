#!/bin/bash
 
# APP info
app_name="Random PWD"
app_version="1.0.0"
app_author="Security Null"
app_last_update="2013-03-12"
app_usage="Usage: ./random_pwd.sh [password length]"
 
function randPassword() {
        MATRIX="abcdefghijklmnopqrstuvwxyzABCDEFGHIJLKMNOPQRSTUVWXYZ-0123456789"
        PASS=""
        n=1
        i=1
        [ -z "$1" ] && length=8 || length=$1
        [ -z "$2" ] && num=1 || num=$2
        while [ ${i} -le $num ]; do
                while [ ${n} -le $length ]; do
                        PASS="$PASS${MATRIX:$(($RANDOM%${#MATRIX})):1}"
                        n=$(($n + 1))
                done
                echo $PASS
                n=1
                PASS=""
                i=$(($i + 1))
        done
}
 
# Check Params
if [ $# -eq 1 ]; then
        rand_pwd=$(randPassword $1)
 
        # Print password
        echo $rand_pwd
else
        echo $app_usage
fi