wget https://nodejs.org/dist/v0.12.7/node-v0.12.7-linux-x64.tar.gz &&
tar xvf node-v0.12.7-linux-x64.tar.gz &&
sudo mv node-v0.12.7-linux-x64 /opt &&
echo "export PATH=\$PATH:/opt/node-v0.12.7-linux-x64/bin" >> ~/.bashrc &&
source ~/.bashrc &&
node --version &&
whereis node &&
dnf -y install npm &&
dnf -y install mondodb &&
dnf -y install git