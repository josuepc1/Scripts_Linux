#!/bin/sh
echo ""
echo "AutoInstalador del programa XX.XX v.1.0"
echo ""
#repositorios
sudo add-apt-repository ppa:clipgrab-team/ppa
sudo add-apt-repository ppa:ubuntu-wine/ppa
sudo add-apt-repository ppa:atareao/telegram
sudo add-apt-repository ppa:relan/exfat
sudo add-apt-repository ppa:desdelinux/viewnior
sudo apt-add-repository ppa:ricotz/docky


#Actualización
sudo apt-get update

#Instalación de aplicaciones
sudo apt-get install mysql-server mysql-client &&
sudo apt-get install clipgrab && 
sudo apt-get install pip &&
sudo apt-get install cmus &&
sudo apt-get install youtube-dl &&
sudo apt-get install wine1.7 winetricks &&
sudo apt-get install telegram && 
sudo apt-get install exfat-utils &&
sudo apt-get install catfish &&
sudo apt-get install git && 
sudo apt-get install plank &&
sudo apt-get install viewnior &&
sudo apt-get install thunderbird thunderbird-locale-es &&
sudo apt-get install finch &&
sudo apt-get install nmap &&

#remover basura
sudo apt-get update
sudo apt-get autoremove
