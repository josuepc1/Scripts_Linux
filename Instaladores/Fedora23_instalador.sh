#!/bin/sh
# -*- ENCODING: UTF-8 -*-
echo ""
echo "AutoInstalador del programa Fedora v.1.0"
echo ""

#actualizacion de inicio
sudo cp *.repo /etc/yum.repos.d/ &&
dnf -y update &&
dnf -y upgrade &&

#Configurar DNF
sudo dnf -y install yumex dnf-plugins-core &&
sudo dnf -y update

#Instalar repositorio ruso y RPM Fusion

sudo dnf -y install --nogpgcheck http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm &&
sudo dnf -y install http://mirror.yandex.ru/fedora/russianfedora/russianfedora/free/fedora/russianfedora-free-release-stable.noarch.rpm && 
sudo dnf -y install http://mirror.yandex.ru/fedora/russianfedora/russianfedora/nonfree/fedora/russianfedora-nonfree-release-stable.noarch.rpm &&
#INSTALACION
su -c "dnf -y install wget && wget -P /etc/yum.repos.d/ https://raw.github.com/kuboosoft/postinstallerf/master/postinstallerf.repo && dnf -y install postinstallerf" &&


#HERRAMIENTAS BASICA

sudo dnf -y install kernel-headers &&
sudo dnf -y install kernel-devel &&
sudo dnf -y groupinstall "Development Tools" &&
sudo dnf -y groupinstall "Development Libraries" &&
sudo dnf -y install dkms &&

#Instalar navegador
sudo dnf -y install https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm &&

#adobe x64
dnf -y install http://linuxdownload.adobe.com/adobe-release/adobe-release-x86_64-1.0-1.noarch.rpm &&
dnf -y install flash-plugin &&

#Extrarepositorios
sudo dnf -y install tidy thrift OpenEXR &&

#compresores
sudo dnf -y install unzip zip &&
dnf -y install unrar p7zip p7zip-plugins &&

#java
sudo dnf -y install java-1.8.0-openjdk java-1.8.0-openjdk-devel icedtea-web &&

#basicos
dnf -y install liveusb-creator transmageddon filezilla pitivi gedit wget curl terminator uget synaptic &&

#correo
dnf -y install thunderbird &&

#nstalación de Codecs necesarios para reproducir música, vídeos, imágenes, etcétera.
sudo dnf -y install gstreamer1-libav gstreamer1-plugins-bad-free-extras gstreamer1-plugins-bad-freeworld gstreamer1-plugins-good-extras gstreamer1-plugins-ugly gstreamer-ffmpeg xine-lib-extras xine-lib-extras-freeworld k3b-extras-freeworld gstreamer-plugins-bad gstreamer-plugins-bad-free-extras gstreamer-plugins-bad-nonfree gstreamer-plugins-ugly gstreamer-ffmpeg &&

sudo dnf -y  install gstreamer-plugins-bad  gstreamer-plugins-bad-free-extras  gstreamer-plugins-bad-nonfree  gstreamer-plugins-ugly gstreamer-ffmpeg &&
sudo dnf -y install gstreamer1-libav gstreamer1-plugins-bad-free-extras  gstreamer1-plugins-bad-freeworld gstreamer1-plugins-base-tools gstreamer1-plugins-good-extras gstreamer1-plugins-ugly  gstreamer1-plugins-bad-free gstreamer1-plugins-good  gstreamer1-plugins-base gstreamer1&&
sudo dnf -y install ffmpeg &&
sudo dnf -y install mencoder &&
sudo dnf -y install ffmpeg2theora &&
sudo dnf -y install mplayer cmus &&
sudo dnf -y install libdvdread libdvdnav lsdvd libdvdcss &&

#Instalar algunos programas para diseño
sudo dnf -y install gimp scribus inkscape blender audacity-freeworld calligra-krita shutter pencil &&

#Instalar software de vídeos
sudo dnf -y install vlc clementine soundconverter mediainfo &&

#RED TOR
sudo dnf -y install tor &&

#Descargas de youtube
sudo dnf -y install clipgrab youtube-dl &&

#Software para ripear y DVD
sudo dnf -y install k3b sound-juicer kid3 &&

#Software para aplicación del sistema
sudo dnf -y install gparted nano wget curl smartmontools htop inxi bleachbit firewall-config beesu pysdm &&

#Instalar Wine en caso de que sea necesario
sudo dnf -y install wine cabextract &&
sudo wget http://winetricks.org/winetricks -O /usr/local/bin/winetricks && sudo chmod +x /usr/local/bin/winetricks &&

#Utilidades de Gnome
sudo dnf -y install screenfetch rfkill lsb &&
sudo dnf -y install cheese gnome-shell-extension-common dconf-editor gnome-tweak-tool gtk-murrine-engine* &&

#otras aplicaciones
sudo dnf -y install pdfmod pdfedit openshot soundconverter yumex gnome-tweak-tool variety plank &&

#password y seguridad
sudo dnf -y install keepass nmap &&

#controlador de brillo
wget http://sourceforge.net/projects/postinstaller/files/fedora/releases/23/x86_64/brightness-2.0-1.fc23.noarch.rpm &&
sudo dnf -y install brightness-2.0-1.fc23.noarch.rpm &&

#conky
sudo dnf -y install conky-manager &&

#spotify
sudo dnf -y install wget &&
wget -P /etc/yum.repos.d/ https://raw.github.com/kuboosoft/postinstallerf/master/postinstallerf.repo &&
sudo dnf -y install spotify-client &&

#telegram
sudo dnf copr enable rommon/telegram &&
sudo dnf install telegram-desktop &&

#Libreoffice
sudo dnf -y install libreoffice-writer libreoffice-calc libreoffice-impress libreoffice-draw libreoffice-langpack-es

#

