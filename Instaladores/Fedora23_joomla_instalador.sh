#INSTALACION DE JOOMLA
#VERSION: 3.4.5

hostname &&
ip addr show &&
wget https://github.com/joomla/joomla-cms/releases/download/3.4.5/Joomla_3.4.5-Stable-Full_Package.zip &&
cp Joomla_3.4.5-Stable-Full_Package.zip /var/www/html/ &&
cd /var/www/html &&
unzip Joomla_3.4.5-Stable-Full_Package.zip &&
rm -rf Joomla_3.4.5-Stable-Full_Package.zip &&
chown -R apache:apache /var/www/html/ &&
service httpd restart &&
chkconfig --level 35 httpd on &&
netstat -antp | grep httpd &&
firewall-cmd --zone=public --add-port=80/tcp --permanent &&
firewall-cmd --reload 