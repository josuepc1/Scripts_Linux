sudo dnf -y update &&
#Instalamos el servidor web(apache httpd) la forma más sencilla(todos estos comandos como root)
sudo dnf groupinstall "Web Server" &&

#Después el servidor mariadb(mysql)
sudo dnf install mariadb-server &&

#Iniciamos los servicios
service httpd start &&
service mariadb start &&

#Los configuramos si queremos que inicien automaticamente
chkconfig --level 345 httpd on &&
chkconfig --level 345 mariadb on &&

#Y luego usamos este comando que nos ayuda a configurar mysql 
sudo mysql_secure_installation &&

#Y voilá, con eso debería funcionar
#Para quien quiera instalar phpMyadmin
sudo dnf install phpMyAdmin &&

#php instalacion
echo "<?php phpinfo(); ?>" > /var/www/html/info.php &&
systemctl restart httpd