#!/bin/bash
# listar_dirs_rw_apache.sh
# Script para listar directorios en los que un usuario posee permiso de escritura
# Desarrollado por Linuxito (http://linuxito.com)
#ejemplo ./listar_dirs_rw_apache.sh www-data /var/www

if [ $# -lt 2 ]
then
    echo "uso: $0 <user> <basedir>"
    exit -1
fi

# Directorio desde donde comenzar la búsqueda, pasado como parámetro
BASEDIR=$2

# Usuario con el que corre Apache, pasado como parámetro
$USER=$1

echo "Listado de directorios donde Apache tiene escritura:"
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"

# Buscar directorios
IFS="$(printf '\n\t')"
for DIR in $(find $BASEDIR -type d)
do
    # Determinar si "$USER" u "other" tiene escritura sobre el directorio
    RW=$(getfacl $DIR 2>/dev/null | grep "$USER\|other" | grep "rw" | wc -l)
    # RW > 0 --> $USER y/o other tienen escritura
    # RW = 0 --> ni $USER ni other tienen escritura
    if [[ $RW -gt 0 ]]
    then
        #echo $DIR [SI]
        echo $DIR
    else
        echo $DIR [NO] > /dev/null
    fi
done