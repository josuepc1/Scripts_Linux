#!/bin/bash

#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  

## pequeño script para formatear dispositivos USB  de manera sencilla
## este script funciona para dispositivos con formato fat fat32

     function cambiaNombreDispositivo(){
				## esta funcion se encarga de cambiarle el nombre al dispositivo si lo deseamos.	
					echo digite el nombre  que desea ponerle al dispositivo 
					read nombre
					nombreDispositivo=$nombre 
			}
			
	 function formateo(){
				## esta funcion se encarga de formatear el dispositivo.
					#echo acontinuacion digite la contraseña del superusuario para continuar
					umount $puntoMontaje 	                         ## desmonta el dispositivo
					mkfs.vfat -n $nombreDispositivo $puntoMontaje  ## formatea con el nuevo nombre y el punto de montaje del dispositivo
					
			}		
		
	
		
    echo conecte su llave o dispositivo usb al computador
    
      puntoMontaje=$(mount | grep vfat | cut -c 1-9) ## obtiene el punto de montaje del dispositivo
      #nombreDispositivo=$(df -H | grep $puntoMontaje | cut -c 88-98) ## obtiene el nombre del dispositivo, si es que lo tiene
      # tamanioDispositivo=$(df -H | grep $puntoMontaje | cut -c 58) ## obtiene el tamanio del dispositivo en GB con potencias de 1000 y no 1024
     
    echo el dispositivo que desea formatear se llama $nombreDispositivo  y su tamanio es $tamanioDispositivo GB
    
   
				## pasa a formatear la llave   
				cambiaNombreDispositivo ## llama a la funcion cambia nombre que asigna otra etiqueta al dispositivo
				formateo                ## llama a la funcion que formatea el dispositivo
				echo su dispositivo ha sido formateado correctamente
		    
		    exit
		    

	
	
